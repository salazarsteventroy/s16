// Repetition Control Structures
// Loops - lets us execute code repeatedly in a pre-set number of time or maybe forever
// (infinite loob)

// While loop

/*It takes in an expression/condition before 
proceding in the evaluation of codes*/

/*Syntax:
		while(expression/condition){
			statement/s;
		}

*/

let count = 5;

while (count !== 0){
	console.log("While loop :" + count);

	count --;
}

/*
	count
	5 4 3 2 1 0
*/

let count1 = 1;

while (count1 !== 11){
	console.log("While loop:" + count1)

	count1 ++;
}

// Do-While loop
// At least one code block will be executed before proceeding to the condition
/*Syntax:
		do {
			statement/s;
		} while (expression/condition)
*/

let countThree =5;

do{
	console.log("Do while loop: " + countThree)

	countThree--;

} while(countThree > 0);

let count2 = 1;

do {
	console.log("do while loop: " + count2) 

	count2 ++;
} while (count2 <= 10);

//  for loop - a more flexible looping construct

/* Syntax:
	for (initialization; expression/condition; final expression){
		statement/s;
	}

*/

for (let countFive = 5; countFive > 0; countFive--){
	console.log("For loop" + countFive);
}

/*let number = Number(prompt("give me a number"));

for (let numCount = 1; numCount <= number; numCount ++){
	console.log("hello batch 144");
}*/

let myString = "alex";
/*console.log(myString.lenght);

console.log (myString[2]);*/

for(let x = 0; x < myString.lenght; x++){
	console.log(myString[x]);
}

	// myString.lenght = 4
	// x = 0 1 2 3 4

	// array
	// element - represents the values in the array
	/*index - location of values(elements) in array
	which statrs at index 0.*/
	//elements - a l e x
	//   index - 0 1 2 3

let myName = "Alex";

for(let i = 0; i < myName.length; i++){
	if( myName[i].toLowerCase() == 'a' || myName[i].toLowerCase() == 'e' ||	
		myName[i].toLowerCase() == 'i' || myName[i].toLowerCase() == 'o' ||
		myName[i].toLowerCase() == 'u')
	{
		/*if the letter in the name is a vowel it will print 3*/
		console.log(3);
	}else{
		// will priint in the console if character is a non-vowel
		console.log(myName[i]);
	}
}

/*contnue and break statement
	continue - allows the code to go to the next iteration for the loop
	without finshing the execution of all statements in a code block

	break - used to terminate the current loop once a match has been found
*/

for (let count = 0; count <= 20; count ++){
	// if remainder is equal to 0
	if(count % 2 === 0){
		// tells the code to continue to the next iteration of the llop
		continue;
	}
	/* the current value of count is greater than 10
	*/
	console.log("continue and break: " + count);

	if(count > 10){
		// tells code to terminate
		break
	}
}

let myName = "Alexandro";

for(let i = 0; i < myName.length; i++){
	if( myName[i].toLowerCase() == 'a' || myName[i].toLowerCase() == 'e' ||	
		myName[i].toLowerCase() == 'i' || myName[i].toLowerCase() == 'o' ||
		myName[i].toLowerCase() == 'u')
	{
		/*if the letter in the name is a vowel it will print 3*/
		console.log(3);
	}else{
		// will print in the console if character is a non-vowel
		console.log(myName[i]);
	}
}

